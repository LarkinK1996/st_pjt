package ru.kata.spring.boot_security.demo.service.abstracts;

import ru.kata.spring.boot_security.demo.model.Student;
import ru.kata.spring.boot_security.demo.model.User;

import java.util.List;

public interface ExcelService {
    public User loadAllUsers();
    public void unloadAllUsers(List<Student> students);
}

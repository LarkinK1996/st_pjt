package ru.kata.spring.boot_security.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kata.spring.boot_security.demo.model.Student;
import ru.kata.spring.boot_security.demo.service.abstracts.ExcelService;
import ru.kata.spring.boot_security.demo.service.abstracts.StudentService;

import java.util.List;

@RestController
@RequestMapping("/api/students/")
public class StudentRestController {


    private final StudentService studentService;
    private final ExcelService excelService;

    public StudentRestController(StudentService studentService, ExcelService excelService) {
        this.studentService = studentService;
        this.excelService = excelService;
    }

    @PostMapping("/unload")
    public ResponseEntity loadAllUsersToExcelFile() {
        List<Student> students = studentService.getStudents();
        excelService.unloadAllUsers(students);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

package ru.kata.spring.boot_security.demo.service.abstracts;

import ru.kata.spring.boot_security.demo.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> getStudents();
    void setStudent(Student student);
}

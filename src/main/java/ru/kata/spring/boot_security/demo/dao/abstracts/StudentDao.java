package ru.kata.spring.boot_security.demo.dao.abstracts;

import ru.kata.spring.boot_security.demo.model.Student;

import java.util.List;

public interface StudentDao {
    public List<Student> getStudents();
    void setStudent(Student student);
}

package ru.kata.spring.boot_security.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kata.spring.boot_security.demo.model.Student;
import ru.kata.spring.boot_security.demo.service.abstracts.ExcelService;
import ru.kata.spring.boot_security.demo.service.abstracts.StudentService;

import java.util.List;

@RequestMapping("/user")
@Controller
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService, ExcelService excelService) {
        this.studentService = studentService;
    }

    @GetMapping("/students")
    public String showInfoOfUser(Model model) {
        List<Student> students = studentService.getStudents();
        model.addAttribute("students", students);
        return "user";
    }

}
